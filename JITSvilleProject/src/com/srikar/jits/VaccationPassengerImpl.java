package com.srikar.jits;

class VaccationPassengerImpl extends PassengerImpl {
	
	private boolean news;
	private double cost;
	private PassengerType type;
	private int numbMiles;
	
	public boolean isNews() {
		return news;
	}

	public void setNews(boolean news) {
		this.news = news;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public PassengerType getType() {
		return type;
	}

	public void setType(PassengerType type) {
		this.type = type;
	}

	public int getNumbMiles() {
		return numbMiles;
	}

	public void setNumbMiles(int numbMiles) {
		
		
		try {
			if(numbMiles > 5 && numbMiles < 4000) {
				this.numbMiles = numbMiles;
			}
		}
		catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void passengerFair() {
		this.cost = milesCost(this.getNumbMiles());
	
	}

	@Override
	public int getNumbStops() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setNumbStops(int numbStops) {
		// TODO Auto-generated method stub
		
	}

	

	
}
