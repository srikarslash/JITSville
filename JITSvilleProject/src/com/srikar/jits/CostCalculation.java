package com.srikar.jits;

class CostCalculation {
	
	double milesCost(int miles) {
		return 0.5*miles;
	}
	
	double stopsCost(int stops) {
		return 0.5*stops;
	}

}
