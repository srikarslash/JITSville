package com.srikar.jits;



public class Client {

	public static void main(String[] args) {
		
		PassengerFactory passengerFactory = new PassengerFactory();
		
		Passenger c1 = passengerFactory.getPassanger(PassengerType.COMMON);
		c1.setNumbStops(4);
		c1.setNews(true);
		c1.passengerFair();
		
		Passenger f1 = passengerFactory.getPassanger(PassengerType.FREQUENT);
		f1.setNumbStops(3);
		f1.passengerFair();
		
		Passenger f2 = passengerFactory.getPassanger(PassengerType.FREQUENT);
		f2.setNumbStops(5);
		f2.passengerFair();
		
		Passenger v1 = passengerFactory.getPassanger(PassengerType.VACATION);
		v1.setNumbMiles(90);
		v1.passengerFair();
		
		Passenger v2 = passengerFactory.getPassanger(PassengerType.VACATION);
		v2.setNumbMiles(199);
		v2.passengerFair();
		
		System.out.println(c1.getCost());
		System.out.println(f1.getCost());
		System.out.println(f2.getCost());
		System.out.println(v1.getCost());
		System.out.println(v2.getCost());
		System.out.println("Total = "+(c1.getCost()+f1.getCost()+f2.getCost()+v1.getCost()+v2.getCost()));
		
	}

}
