package com.srikar.jits;

interface Passenger {
	
	void passengerFair();
	boolean isNews();
	void setNews(boolean news);
	double getCost();
	void setCost(double cost);
	PassengerType getType();
	void setType(PassengerType type);
	int getNumbStops();
	void setNumbStops(int numbStops);
	int getNumbMiles();
	void setNumbMiles(int numbMiles);

}
