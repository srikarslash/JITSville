package com.srikar.jits;

class FrequentCommuterPassengerImpl extends PassengerImpl{
	
	private boolean news;
	private double cost;
	private PassengerType type;
	private int numbStops;
	
	public boolean isNews() {
		return news;
	}

	public void setNews(boolean news) {
		this.news = news;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public PassengerType getType() {
		return type;
	}

	public void setType(PassengerType type) {
		this.type = type;
	}
	
	public int getNumbStops() {
		return numbStops;
	}

	public void setNumbStops(int numbStops) {
		this.numbStops = numbStops;
	}
	@Override
	public void passengerFair() {
		double discount = 0.1;
		
		this.cost = (1-discount)*(stopsCost(this.getNumbStops()));
		
	}

	@Override
	public int getNumbMiles() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setNumbMiles(int numbMiles) {
		// TODO Auto-generated method stub
		
	}

	
}
