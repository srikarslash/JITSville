package com.srikar.jits;

public class PassengerFactory {
	
	Passenger getPassanger(PassengerType type){
		
	      if(type == PassengerType.COMMON){
	         return new CommonCommuterPassengerImpl();
	      }		
	      else if(type == PassengerType.FREQUENT){
	         return new FrequentCommuterPassengerImpl();
	         
	      } else if(type == PassengerType.VACATION){
	         return new VaccationPassengerImpl();
	      }
	      
	      return null;
	   }
	
}
